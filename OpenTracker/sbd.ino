
void sbd_init() {
//  return;
  debug_port.println(F("sbd_init() started"));
  sbd_port.begin(19200);
//  isbd.attachConsole(debug_port);
//  isbd.attachDiags(debug_port);
  // isbd.attachConsole(debug_port); temporary turned off loading from eeprom by chaky
  // isbd.attachDiags(debug_port);
  delay(500);
//  isbd.setPowerProfile(1);
  isbd.setPowerProfile(0);
  isbd.useMSSTMWorkaround(false);
  isbd.begin();
  int signalQuality = -1;
  int err = isbd.getSignalQuality(signalQuality);
  if (err != 0){
    debug_port.println(F("SDB ERR1")); 
    debug_port.println(err);
  }else{
    debug_port.println(F("sbd_init signalQuality:"));
    debug_port.println(signalQuality);
  } 
  isbd.sleep();
}


void sdb_loop() {
//  return;
  unsigned long noDataSeconds = (unsigned long)((unsigned long)millis() - lastSendDataMillis);
  noDataSeconds = noDataSeconds/1000;
  if (noDataSeconds > config.sdbSendSeconds){
    sdbTrySendData();
  }
  if (noDataSeconds > NO_DATA_REBOOT_SECONDS){
    power_reboot = 1;
//    reboot2(2);
  }
}

void sdbTrySendData(){
//  return;
  if (isbd.begin() == ISBD_SUCCESS){
    char outBuffer[32]; // Always try to keep message short
    sprintf(outBuffer, "%s,%s", lat_current, lon_current);
    debug_port.println(F("sdbTrySendData outBuffer:"));
    debug_port.println(outBuffer);
    isbd.sendSBDText(outBuffer);
    int err = isbd.sendSBDText(outBuffer);
    if (err != 0)    {
      debug_port.print(F("sendSBDText failed: error:"));
      debug_port.println(err);
      return;
    }
    lastSendDataMillis = millis();
  }else{
    debug_port.println(F("ERROR SDB2 in begin()"));
  }
  isbd.sleep();
}
